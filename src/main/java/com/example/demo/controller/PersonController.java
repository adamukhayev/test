package com.example.demo.controller;

import com.example.demo.model.dto.ContactDto;
import com.example.demo.model.dto.PersonDto;
import com.example.demo.service.ContactService;
import com.example.demo.service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/person")
public class PersonController {

    private final PersonService personService;
    private final ContactService contactService;

    @PostMapping(path = "/add")
    @ResponseStatus(HttpStatus.OK)
    public void add(@RequestBody PersonDto personDto) {

        personService.add(personDto);
    }
    @GetMapping(path = "/get/all")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<PersonDto>> getAllPerson(){

        return ResponseEntity.ok(personService.getAll());
    }

    @GetMapping(path = "/get")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<PersonDto> getPerson(@RequestParam(value = "id",required = false) Integer id){
        PersonDto personDto = personService.get(id);

        return ResponseEntity.ok(personDto);
    }

    @PutMapping(path = "/update")
    @ResponseStatus(HttpStatus.OK)
    public void updatePerson(@RequestBody PersonDto personDto){
             personService.update(personDto);
    }

    @DeleteMapping(path = "/del")
    @ResponseStatus(HttpStatus.OK)
    public void deletion(@RequestParam(value = "id",required = false) Integer id){
        personService.deletion(id);
    }

    @DeleteMapping(path = "/del/cont")
    @ResponseStatus(HttpStatus.OK)
    public void deletionContact(@RequestParam(value = "contact_id",required = false) Integer contactId,
                                @RequestParam(value = "person_id",required = false) Integer personId ){
        contactService.deleteContact(contactId,personId);
    }

    @PostMapping(path = "/add/contact")
    @ResponseStatus(HttpStatus.OK)
    public void addContact(@RequestBody ContactDto contactDto){
        contactService.add(contactDto);
    }

    @PutMapping(path = "/update/contact")
    @ResponseStatus(HttpStatus.OK)
    public void updateContact(@RequestBody ContactDto contactDto){
        contactService.update(contactDto);
    }

}
