package com.example.demo.repository;

import com.example.demo.model.entity.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Repository
public interface PersonRepository extends JpaRepository<PersonEntity,Integer> {

    @Transactional
    @Modifying
    @Query("update PersonEntity person " +
            "set person.name = :name, " +
            "person.surname = :surname," +
            "person.dateOfChange = :dateOfChange " +
            "where person.id = :id")
    void update(@Param("id")Integer id,
                @Param("name") String name,
                @Param("surname") String surname,
                @Param("dateOfChange")Date dateOfChange);


    PersonEntity findAllById(Integer id);
}
