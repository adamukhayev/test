package com.example.demo.service;

import com.example.demo.model.dto.ContactDto;
import com.example.demo.model.dto.PersonDto;
import com.example.demo.model.entity.PersonEntity;
import com.example.demo.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class PersonService {
    private final PersonRepository personRepository;
    private final ContactService contactService;
    private final ModelMapper modelMapper;

    public void add(PersonDto personDto) {
        Date date = new Date();
        personDto.setDateOfCreation(date);
        PersonDto dto = modelMapper.map(personRepository.save(modelMapper.map(personDto, PersonEntity.class)),PersonDto.class);

        if(Objects.nonNull(personDto.getContactDto())) {
            for (int i = 0; i < personDto.getContactDto().size(); i++) {
                personDto.getContactDto().get(i).setPersonId(dto.getId());
                contactService.add(personDto.getContactDto().get(i));
            }
        }
    }

    public List<PersonDto> getAll() {
        List<PersonEntity> entitis = personRepository.findAll();

        List<PersonDto> dtos = entitis.stream()
                .map(item -> modelMapper.map(item, PersonDto.class))
                .collect(Collectors.toList());


        for (int i = 0; i < dtos.size(); i++) {
            dtos.get(i).setContactDto(contactService.get(dtos.get(i).getId()));
        }
        return dtos;
    }

    public PersonDto get(Integer id) {
        try {
           PersonDto personDtos = modelMapper.map(personRepository.findAllById(id), PersonDto.class);
            personDtos.setContactDto(contactService.get(personDtos.getId()));
            return personDtos;
        }catch (Exception e){
            log.error("the list is empty ");
        }
        return null;
    }

    @Transactional
    public void deletion(Integer id) {
        contactService.delete(id);
        personRepository.deleteById(id);
    }

    public void update(PersonDto personDto) {
        Date date = new Date();
        personDto.setDateOfChange(date);
        PersonEntity personEntity = modelMapper.map(personDto,PersonEntity.class);
        personRepository.update(personEntity.getId(),personEntity.getName(),personEntity.getSurname(),
                personEntity.getDateOfChange());

        if (Objects.nonNull(personDto.getContactDto())) {
            for (int i = 0; i < personDto.getContactDto().size(); i++) {
                contactService.update(personDto.getContactDto().get(i));
            }
        }
    }

}
