package com.example.demo.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "contact")
public class ContactEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "contact_value")
    private String contactValue;

    @Column(name = "contact_type")
    private String contactType;

    @Column(name = "date_of_creation")
    private Date dateOfCreation;

    @Column(name = "date_of_change")
    private Date dateOfChange;

    @Column(name = "person_id")
    private Integer personId;
}
