package com.example.demo.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ContactDto {

    private Integer id;
    private String contactValue;
    private String contactType;
    private Date dateOfCreation;
    private Date dateOfChange;
    private Integer personId;
}
