package com.example.demo.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PersonDto {

    private Integer id;
    private String name;
    private String surname;
    private Date dateOfCreation;
    private Date dateOfChange;
    private List <ContactDto> contactDto;
}
