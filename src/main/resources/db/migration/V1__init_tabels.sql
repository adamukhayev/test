CREATE TABLE person(
    id         SERIAL PRIMARY KEY,
    name      CHARACTER VARYING(128) NOT NULL,
    surname   CHARACTER VARYING(128) NOT NULL,
    date_of_creation  TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
    date_of_change TIMESTAMP WITHOUT TIME ZONE DEFAULT now()
);
CREATE TABLE contact(
    id SERIAL PRIMARY KEY,
    contact_value CHARACTER VARYING(200),
    contact_type  CHARACTER VARYING (20),
    date_of_creation TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
    date_of_change TIMESTAMP WITHOUT TIME ZONE DEFAULT now() ,
    person_id INTEGER REFERENCES person(id)
)