package com.example.demo.service;

import com.example.demo.model.dto.ContactDto;
import com.example.demo.model.entity.ContactEntity;
import com.example.demo.repository.ContactRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class ContactService {

    private final ContactRepository contactRepository;
    private final ModelMapper modelMapper;
    public List<ContactDto> get(Integer id) {
            List<ContactEntity> listContact = contactRepository.findAllByPersonId(id);

            List<ContactDto> contactDto = listContact.stream()
                    .map(item -> modelMapper.map(item, ContactDto.class))
                    .collect(Collectors.toList());

            return contactDto;
    }

    @Transactional
    public void delete(Integer id) {
        contactRepository.deleteAllByPersonId(id);
    }

    public void add(ContactDto contactDto) {
        Date date = new Date();
        contactDto.setDateOfCreation(date);
            contactRepository.save(modelMapper.map(contactDto,ContactEntity.class));
    }

    @Transactional
    public void update(ContactDto contactDto) {
        Date date = new Date();
        contactDto.setDateOfChange(date);
        ContactEntity contactEntity = modelMapper.map(contactDto,ContactEntity.class);
        contactRepository.update(contactEntity.getId(),contactEntity.getPersonId(),
                contactEntity.getContactValue(),contactEntity.getContactType(),
                contactEntity.getDateOfChange());
    }

    @Transactional
    public void deleteContact(Integer idContact, Integer idPerson) {
        contactRepository.deleteByIdAndPersonId(idContact,idPerson);
    }
}
