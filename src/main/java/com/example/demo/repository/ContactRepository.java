package com.example.demo.repository;


import com.example.demo.model.entity.ContactEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<ContactEntity,Integer> {

    List<ContactEntity> findAllByPersonId(Integer id);

    void deleteAllByPersonId(Integer id);

    @Modifying
    @Query("update ContactEntity contact " +
            "set contact.contactValue = :contactValue, " +
            "contact.contactType = :contactType," +
            "contact.dateOfChange = :dateOfCange " +
            "where contact.id = :id and contact.personId = :personId")
    void update(Integer id, Integer personId, String contactValue, String contactType, Date dateOfCange);

    void deleteByIdAndPersonId(Integer idContact, Integer idPerson);
}
